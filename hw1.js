const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var ar = [], order = [0, 1, 2], op = [], val = [];
var NOVAL = 9999, oper = "+-*/", out;

function rnd(n) {
    return Math.floor(Math.random() * n)
}

function getvalue(x, dir) {
    var r = NOVAL;
    if (dir > 0) ++x;
    while (1) {
        if (val[x] != NOVAL) {
            r = val[x];
            val[x] = NOVAL;
            break;
        }
        x += dir;
    }
    return r * 1;
}

function calc() {
    var c = 0, l, r, x;
    val = ar.join('/').split('/');
    while (c < 3) {
        x = order[c];
        l = getvalue(x, -1);
        r = getvalue(x, 1);
        switch (op[x]) {
            case 0: val[x] = l + r; break;
            case 1: val[x] = l - r; break;
            case 2: val[x] = l * r; break;
            case 3:
                if (!r || l % r) return 0;
                val[x] = l / r;
        }
        ++c;
    }
    return getvalue(-1, 1);
}

function shuffle(s, n) {
    var x = n, p = eval(s), r, t;
    while (x--) {
        r = rnd(n);
        t = p[x];
        p[x] = p[r];
        p[r] = t;
    }
}

function parenth(n) {
    while (n > 0) --n, out += '(';
    while (n < 0) ++n, out += ')';
}

function getpriority(x) {
    for (var z = 3; z--;)if (order[z] == x) return 3 - z;
    return 0;
}

function showsolution() {
    var x = 0, p = 0, lp = 0, v = 0;
    while (x < 4) {
        if (x < 3) {
            lp = p;
            p = getpriority(x);
            v = p - lp;
            if (v > 0) parenth(v);
        }
        out += ar[x];
        if (x < 3) {
            if (v < 0) parenth(v);
            out += oper.charAt(op[x]);
        }
        ++x;
    }
    parenth(-p);
    return `Success : Result is ${out}`;
}

function solve24(s) {
    var z = 4, r;
    while (z--) ar[z] = s.charCodeAt(z) - 48;
    out = "";
    for (z = 100000; z--;) {
        r = rnd(256);
        op[0] = r & 3;
        op[1] = (r >> 2) & 3;
        op[2] = (r >> 4) & 3;
        shuffle("ar", 4);
        shuffle("order", 3);
        if (calc() != 24) continue;
        else return showsolution();
        break;
    }
}

app.get("/:number1/:number2/:number3/:number4", function (req, res) {
    var data = {
        number1: req.params.number1,
        number2: req.params.number2,
        number3: req.params.number3,
        number4: req.params.number4,
    };

    if (data.number1 < 1 || data.number1 > 9) {
        res.status(403).send({ error: "Invalid input number 1" });
    }
    if (data.number2 < 1 || data.number2 > 9) {
        res.status(403).send({ error: "Invalid input number 2" });
    }
    if (data.number3 < 1 || data.number3 > 9) {
        res.status(403).send({ error: "Invalid input number 3" });
    }
    if (data.number4 < 1 || data.number4 > 9) {
        res.status(403).send({ error: "Invalid input number 4" });
    } else {
        var result = solve24(`${data.number1}${data.number2}${data.number3}${data.number4}`);
        if (result === undefined) {
            res.send("Fail");
        } else {
            res.send(result);
        }
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});
